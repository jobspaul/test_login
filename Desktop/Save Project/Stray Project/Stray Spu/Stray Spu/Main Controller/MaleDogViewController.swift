//
//  MaleDogViewController.swift
//  Stray Spu
//
//  Created by Nattawut Nokyoo on 11/29/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import UIKit
import Firebase

class MaleDogViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var data: [DataModel] = []
    var ref: DatabaseReference!
    var dataSelect: DataModel?
    var selectData: DataModel?
    
     @IBOutlet weak var myTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.readText()
        self.title = "Breed"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    func readText() {
        
        ref = Database.database().reference()
        ref.child("dogM").observe(.childAdded, with: {(DataSnapshot) in
            //ref.observe(.childAdded, with: {(DataSnapshot) in
            //debugPrint("Test Data : \(DataSnapshot)")
            let value = DataSnapshot.value as? NSDictionary
            let username = value?["Name"] as? String ?? ""
            let userId = value?["AnimalID"] as? String ?? ""
            let userBreed = value?["Breed"] as? String ?? ""
            let userColer = value?["Color"] as? String ?? ""
            let userDate = value?["DateTime"] as? String ?? ""
            let userTime = value?["AgeuponOutcome"] as? String ?? ""
            let userSex = value?["SexuponOutcome"] as? String ?? ""
            
            let item = DataModel()
            item.myList3 = username
            item.myListId3 = userId
            item.myListBreed3 = userBreed
            item.myListColor3 = userColer
            item.myListDate3 = userDate
            item.myListTime3 = userTime
            item.myListSex3 = userSex
            self.data.append(item)
            //debugPrint("Error:\(item)")
            
            
            self.myTableView.reloadData()
            
        }) { (error) in
            debugPrint("error : \(error.localizedDescription)")
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MaleDogTableViewCell
        let item = self.data[indexPath.row]
        cell.myBreedLabel.text = item.myListColor3
        cell.myLabel.text = item.myListBreed3
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        dataSelect = self.data[indexPath.row]
        self.performSegue(withIdentifier: "goToShowDetailDogM", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToShowDetailDogM" {
            if let vc = segue.destination as? ShowDetailDogMViewController {
                vc.dataPass = self.dataSelect
            }
        }
        
    }
}
