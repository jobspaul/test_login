//
//  informationViewController.swift
//  Stray Spu
//
//  Created by Nattawut Nokyoo on 11/29/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import UIKit

class informationViewController: UIViewController {
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var detailTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.detailTextView.text = "Both cats and dogs are commonly adopted  or transferred (cats moreso), but dogs are much more likely to be returned to their owners than cats. It also appears that cats are more likely to have died compared to dogs. Fortunately, it appears very few animals die or get euthanized overall"
        self.title = "Information"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "backTomenu", sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
