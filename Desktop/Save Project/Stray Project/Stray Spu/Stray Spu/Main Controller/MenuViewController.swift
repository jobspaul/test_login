//
//  MenuViewController.swift
//  Stray Spu
//
//  Created by Nattawut Nokyoo on 11/25/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
   
    @IBOutlet weak var dogBth: UIButton!
    @IBOutlet weak var catBth: UIButton!
    @IBOutlet weak var informationBtn: UIButton!
    @IBOutlet weak var testBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true;
        self.title = "Menu"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goToPageDog(_ sender: UIButton) {
        self.performSegue(withIdentifier: "goToDog", sender: self)
    
    }
    
    @IBAction func goToPageCat(_ sender: UIButton) {
        self.performSegue(withIdentifier: "goToCat", sender: self)
    }
    @IBAction func goToInformation(_ sender: UIButton) {
        self.performSegue(withIdentifier: "goToInformation", sender: self)
    }
    @IBAction func goToTest(_ sender: UIButton) {
        self.performSegue(withIdentifier: "goToBackMainMenu", sender: self)
    }
    
 }
