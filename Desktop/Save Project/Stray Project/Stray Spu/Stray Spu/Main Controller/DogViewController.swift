//
//  DogViewController.swift
//  Stray Spu
//
//  Created by Nattawut Nokyoo on 11/25/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import UIKit

class DogViewController: UIViewController {
    @IBOutlet weak var maleDogBtn: UIButton!
    @IBOutlet weak var femaleDogBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false;
        self.title = "Gender"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionToMaleDog(_ sender: UIButton) {
        self.performSegue(withIdentifier: "goToMaleDog", sender: self)
    }
    
    @IBAction func actionToFemaleDog(_ sender: UIButton) {
        self.performSegue(withIdentifier: "goToFemaleDog", sender: self)
    }
    
}
