//
//  FemaleDogViewController.swift
//  Stray Spu
//
//  Created by Nattawut Nokyoo on 11/29/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import UIKit
import Firebase

class FemaleDogViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var myTableView: UITableView!
    
    var data: [DataModel] = []
    var dataSelect: DataModel?
    var ref: DatabaseReference!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.readText()
        self.title = "Breed"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func readText() {
        
        ref = Database.database().reference()
        ref.child("dogF").observe(.childAdded, with: {(DataSnapshot) in
            //ref.observe(.childAdded, with: {(DataSnapshot) in
            //debugPrint("Test Data : \(DataSnapshot)")
            let value = DataSnapshot.value as? NSDictionary
            let username = value?["Name"] as? String ?? ""
            let userId = value?["AnimalID"] as? String ?? ""
            let userBreed = value?["Breed"] as? String ?? ""
            let userColer = value?["Color"] as? String ?? ""
            let userDate = value?["DateTime"] as? String ?? ""
            let userTime = value?["AgeuponOutcome"] as? String ?? ""
            let userSex = value?["SexuponOutcome"] as? String ?? ""
            
            let item = DataModel()
            item.myList4 = username
            item.myListId4 = userId
            item.myListBreed4 = userBreed
            item.myListColor4 = userColer
            item.myListDate4 = userDate
            item.myListTime4 = userTime
            item.myListSex4 = userSex
            self.data.append(item)
            self.myTableView.reloadData()
            
        }) { (error) in
            debugPrint("error : \(error.localizedDescription)")
        }
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FemaleDogTableViewCell
        let item = self.data[indexPath.row]
        cell.myLabel.text = item.myListBreed4
        cell.myBreedLabel.text = item.myListColor4

        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        dataSelect = self.data[indexPath.row]
        self.performSegue(withIdentifier: "goToShowDetailDogF", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToShowDetailDogF" {
            if let vc = segue.destination as? ShowDetailDogFViewController {
                vc.dataPass = self.dataSelect
            }
        }
        
    }
}

