//
//  MaleCatViewController.swift
//  Stray Spu
//
//  Created by Nattawut Nokyoo on 11/25/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import UIKit
import Firebase

class MaleCatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var myTableView: UITableView!

    var data: [DataModel] = []
    var dataSelect: DataModel?
    //var myListID:[String] = []
    
    
   // var handle: DatabaseHandle!
    var ref: DatabaseReference!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.readText()
        self.title = "Breed"
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func readText() {
        
        ref = Database.database().reference()
        ref.child("catM").observe(.childAdded, with: {(DataSnapshot) in
        //ref.observe(.childAdded, with: {(DataSnapshot) in
        //debugPrint("Test Data : \(DataSnapshot)")
            let value = DataSnapshot.value as? NSDictionary
            let username = value?["Name"] as? String ?? ""
            let userId = value?["AnimalID"] as? String ?? ""
            let userBreed = value?["Breed"] as? String ?? ""
            let userColer = value?["Color"] as? String ?? ""
            let userDate = value?["DateTime"] as? String ?? ""
            let userTime = value?["AgeuponOutcome"] as? String ?? ""
            let userSex = value?["SexuponOutcome"] as? String ?? ""
            
            let item = DataModel()
            item.myList = username
            item.myListId = userId
            item.myListBreed = userBreed
            item.myListColor = userColer
            item.myListDate = userDate
            item.myListTime = userTime
            item.myListSex = userSex
            self.data.append(item)
            self.myTableView.reloadData()
            
        }) { (error) in
            debugPrint("error : \(error.localizedDescription)")
        }
        
    //Test
    /*ref = Database.database().reference()
        handle = ref.child("99").observe(.childAdded, with: {(DataSnapshot) in
        //ref.queryOrdered(byChild: "AnimalID").queryEqual(toValue: "A665666").observeSingleEvent(of: .value, with: {(DataSnapshot) in
            
            if let item = DataSnapshot.value as? String {
                self.myList.append(item)
                self.myTableView.reloadData()
            }
        })*/
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MaleCatViewControllTableViewCell
        let item = self.data[indexPath.row]
        cell.myLabel.text = item.myListColor
        //cell.myIdLabel.text = item.myListId
        cell.myBreedLabel.text = item.myListBreed
        
        //cell.myLabel.text = myList[indexPath.row]
        //cell.myIdLabel.text = myList[indexPath.row]
        
       return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        dataSelect = self.data[indexPath.row]
        self.performSegue(withIdentifier: "goToShowDetailCatM", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToShowDetailCatM" {
            if let vc = segue.destination as? ShowDetailViewController {
                vc.dataPass = self.dataSelect
            }
        }
        
    }
}
