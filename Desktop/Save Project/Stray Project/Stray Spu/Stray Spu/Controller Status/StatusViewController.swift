//
//  StatusViewController.swift
//  Stray Spu
//
//  Created by Nattawut Nokyoo on 12/4/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import UIKit
import Firebase

class StatusViewController: UIViewController {

    
    var dataVale:[DataModelStatus] = []
    var ref: DatabaseReference!
    var dataSelect:DataModelStatus?
    
    @IBOutlet weak var showLb: UILabel!
    @IBOutlet weak var showCountLb: UILabel!
   
    @IBOutlet weak var showLb2: UILabel!
    @IBOutlet weak var showCountLb2: UILabel!
 
    @IBOutlet weak var showLb3: UILabel!
    @IBOutlet weak var showCountLb3: UILabel!
    
    @IBOutlet weak var titleLb: UILabel!
    @IBOutlet weak var titleBarLb: UILabel!
    
    @IBOutlet weak var titleLb1: UILabel!
    @IBOutlet weak var titleBarLb1: UILabel!
    
    @IBOutlet weak var titleLb3: UILabel!
    @IBOutlet weak var titleBarLb3: UILabel!
    
    @IBOutlet var showLb4: UILabel!
    @IBOutlet var showLbDetail4: UILabel!
    
    @IBOutlet weak var bgImage: UIImageView!
    @IBOutlet weak var bgImage2: UIImageView!
    @IBOutlet weak var bgImage3: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Outcome"
        //self.showLb.isHidden = true
        //self.readText()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goToAdoption(_ sender: UIButton) {
        self.showLb.isHidden = false
        self.showCountLb.isHidden = false
        self.showLb2.isHidden = true
        self.showCountLb2.isHidden = true
        self.showLb3.isHidden = true
        self.showCountLb3.isHidden = true
        
        self.titleLb.isHidden = false
        self.titleBarLb.isHidden = false
        self.titleLb1.isHidden = true
        self.titleBarLb1.isHidden = true
        self.titleLb3.isHidden = true
        self.titleBarLb3.isHidden = true
        
        self.showLb4.isHidden = true
        self.showLbDetail4.isHidden = true
        
        self.bgImage.isHidden = false
        self.bgImage2.isHidden = true
        self.bgImage3.isHidden = true
        
        self.alertShow()
        
        
        
        ref = Database.database().reference()
        ref.child("dataAnimal").observe(.childAdded, with: {(DataSnapshot) in
            //ref.observe(.childAdded, with: {(DataSnapshot) in
            //debugPrint("Test Data : \(DataSnapshot)")
            let value = DataSnapshot.value as? NSDictionary
            let type = value?["OutcomeType"] as? String ?? ""
            let animal = value?["AnimalType"] as? String ?? ""
            let subtype = value?["OutcomeSubtype"] as? String ?? ""
            let subtypeSex = value?["SexuponOutcome"] as? String ?? ""
            let subtypeAge = value?["AgeuponOutcome"] as? String ?? ""
            
            let item = DataModelStatus()
            item.outComeType = type
            item.animalType = animal
            item.outComeSubtype = subtype
            item.sexUponOutcome = subtypeSex
            item.subTypeAge = subtypeAge
            self.dataVale.append(item)
            //print("Error1:\(item.outComeType)")
            
            if (item.outComeType == "Adoption") {
                if (item.sexUponOutcome == "Spayed Female") {
                    //let countSex = item.sexUponOutcome?.count
                    self.showLb.text = item.sexUponOutcome
                    self.showCountLb.text = ("All Animals")
                    
                    self.showCountLb2.text = ""
                    self.showLb2.text = ""
                   
                    self.showCountLb3.text = ""
                    self.showLb3.text = ""
                }
            }
            
            
        }) { (error) in
            debugPrint("error : \(error.localizedDescription)")
        }
        //self.performSegue(withIdentifier: "goToShowStatus", sender: self)
    }
    
    @IBAction func goToEuthanasia(_ sender: UIButton) {
        self.showLb.isHidden = false
        self.showCountLb.isHidden = false
        self.showLb2.isHidden = false
        self.showCountLb2.isHidden = false
        self.showLb3.isHidden = false
        self.showCountLb3.isHidden = false
        
        self.titleLb.isHidden = false
        self.titleBarLb.isHidden = false
        self.titleLb1.isHidden = false
        self.titleBarLb1.isHidden = false
        self.titleLb3.isHidden = false
        self.titleBarLb3.isHidden = false
        
        self.showLb4.isHidden = false
        self.showLbDetail4.isHidden = false
        
        self.bgImage.isHidden = true
        self.bgImage2.isHidden = true
        self.bgImage3.isHidden = false
        
        self.alertShow()
        
        ref = Database.database().reference()
        ref.child("dataAnimal").observe(.childAdded, with: {(DataSnapshot) in
            //ref.observe(.childAdded, with: {(DataSnapshot) in
            //debugPrint("Test Data : \(DataSnapshot)")
            let value = DataSnapshot.value as? NSDictionary
            let type = value?["OutcomeType"] as? String ?? ""
            let animal = value?["AnimalType"] as? String ?? ""
            let subtype = value?["OutcomeSubtype"] as? String ?? ""
            let subtypeSex = value?["SexuponOutcome"] as? String ?? ""
            let subtypeAge = value?["AgeuponOutcome"] as? String ?? ""
            
            let item = DataModelStatus()
            item.outComeType = type
            item.animalType = animal
            item.outComeSubtype = subtype
            item.sexUponOutcome = subtypeSex
            item.subTypeAge = subtypeAge
            self.dataVale.append(item)
            //print("Error1:\(item.outComeType)")
            
            if (item.outComeType == "Euthanasia") {
                if (item.animalType == "Cat") {
                    if (item.sexUponOutcome == "Spayed Female") {
                        //let countSex = item.sexUponOutcome?.count
                        self.showLb.text = item.sexUponOutcome
                        self.showCountLb.text = item.animalType

                }
              }
               else if (item.animalType == "Dog") {
                    if (item.outComeSubtype == "Aggressive") {
                        if (item.sexUponOutcome == "Spayed Female") {
                            //let countSex = item.sexUponOutcome?.count
                            self.showCountLb2.text = item.animalType
                            self.showLb2.text = item.sexUponOutcome
                            self.showLb4.text = item.outComeSubtype
                            
                        }
                        
                    }
                    else if (item.outComeSubtype == "Suffering") {
                        if (item.sexUponOutcome == "Neutered Male") {
                            self.showCountLb3.text = item.animalType
                            self.showLb3.text = item.sexUponOutcome
                            self.showLbDetail4.text = item.outComeSubtype
                        }
                    }
                    
                }
                
            }
        }) { (error) in
            debugPrint("error : \(error.localizedDescription)")
        }
    }
    
    @IBAction func goToReturn(_ sender: UIButton) {
        //self.alertShow()
        self.showLb.isHidden = false
        self.showCountLb.isHidden = false
        self.showLb2.isHidden = true
        self.showCountLb2.isHidden = true
        self.showLb3.isHidden = true
        self.showCountLb3.isHidden = true
        
        self.titleLb.isHidden = false
        self.titleBarLb.isHidden = false
        self.titleLb1.isHidden = true
        self.titleBarLb1.isHidden = true
        self.titleLb3.isHidden = true
        self.titleBarLb3.isHidden = true
        
        self.showLb4.isHidden = true
        self.showLbDetail4.isHidden = true
        
        self.bgImage.isHidden = false
        self.bgImage2.isHidden = true
        self.bgImage3.isHidden = true
        
        self.alertShow()
        
        ref = Database.database().reference()
        ref.child("dataAnimal").observe(.childAdded, with: {(DataSnapshot) in
            //ref.observe(.childAdded, with: {(DataSnapshot) in
            //debugPrint("Test Data : \(DataSnapshot)")
            let value = DataSnapshot.value as? NSDictionary
            let type = value?["OutcomeType"] as? String ?? ""
            let animal = value?["AnimalType"] as? String ?? ""
            let subtype = value?["OutcomeSubtype"] as? String ?? ""
            let subtypeSex = value?["SexuponOutcome"] as? String ?? ""
            let subtypeAge = value?["AgeuponOutcome"] as? String ?? ""
            
            let item = DataModelStatus()
            item.outComeType = type
            item.animalType = animal
            item.outComeSubtype = subtype
            item.sexUponOutcome = subtypeSex
            item.subTypeAge = subtypeAge
            self.dataVale.append(item)
            //print("Error1:\(DataModelStatus().count)")
            
            if (item.outComeType == "Return_to_owner") {
                if (item.sexUponOutcome == "Neutered Male") {
                    //let countSex = item.sexUponOutcome?.count
                    self.showLb.text = item.sexUponOutcome
                    self.showCountLb.text = ("All Animals")
                    //self.showDetailLb.text = ""
                    self.showCountLb2.text = ""
                    self.showLb2.text = ""
                    //self.showDetailLb2.text = ""
                    self.showCountLb3.text = ""
                    self.showLb3.text = ""
                }
            }
            
        }) { (error) in
            debugPrint("error : \(error.localizedDescription)")
        }
    }
    
    @IBAction func goToTranfer(_ sender: UIButton) {
        self.showLb.isHidden = false
        self.showCountLb.isHidden = false
        self.showLb2.isHidden = false
        self.showCountLb2.isHidden = false
        self.showLb3.isHidden = true
        self.showCountLb3.isHidden = true
        
        self.titleLb.isHidden = false
        self.titleBarLb.isHidden = false
        self.titleLb1.isHidden = false
        self.titleBarLb1.isHidden = false
        self.titleLb3.isHidden = true
        self.titleBarLb3.isHidden = true
        
        self.showLb4.isHidden = true
        self.showLbDetail4.isHidden = true
        
        self.bgImage.isHidden = true
        self.bgImage2.isHidden = false
        self.bgImage3.isHidden = true
        
        self.alertShow()
        
        
        ref = Database.database().reference()
        ref.child("dataAnimal").observe(.childAdded, with: {(DataSnapshot) in
            //ref.observe(.childAdded, with: {(DataSnapshot) in
            //debugPrint("Test Data : \(DataSnapshot)")
            let value = DataSnapshot.value as? NSDictionary
            let type = value?["OutcomeType"] as? String ?? ""
            let animal = value?["AnimalType"] as? String ?? ""
            let subtype = value?["OutcomeSubtype"] as? String ?? ""
            let subtypeSex = value?["SexuponOutcome"] as? String ?? ""
            let subtypeAge = value?["AgeuponOutcome"] as? String ?? ""
            
            let item = DataModelStatus()
            item.outComeType = type
            item.animalType = animal
            item.outComeSubtype = subtype
            item.sexUponOutcome = subtypeSex
            item.subTypeAge = subtypeAge
            self.dataVale.append(item)
            //print("Error1:\(item.outComeType)")
            
            if (item.outComeType == "Transfer") {
                    if (item.animalType == "Cat") {
                        if (item.sexUponOutcome == "Intact Male") {
                            //print("Error")
                            self.showLb.text = item.sexUponOutcome
                            self.showCountLb.text = item.animalType
                            //self.showDetailLb.text = ""
                            self.showCountLb2.text = ""
                            //self.showLb2.text = ""
                            //self.showDetailLb2.text = ""
                            self.showCountLb3.text = ""
                            self.showLb3.text = ""
                        }
                    }
                    else if (item.animalType == "Dog") {
                        //self.showDetailLb.text = item.sexUponOutcome
                        self.showLb2.text = item.sexUponOutcome
                        //self.showDetailLb.text = ""
                        self.showCountLb2.text = item.animalType
                        //self.showLb2.text = ""
                        //self.showDetailLb2.text = ""
                        self.showCountLb3.text = ""
                        self.showLb3.text = ""
                    }
                    
                }
            
        }) { (error) in
            debugPrint("error : \(error.localizedDescription)")
        }
        
    }
    
    func alertShow () {
        let alertView = UIAlertView(title: "Prediction ", message: "connect to internet", delegate: "cancel", cancelButtonTitle: "OK")
        alertView.show()
    }
    
    
    
}

