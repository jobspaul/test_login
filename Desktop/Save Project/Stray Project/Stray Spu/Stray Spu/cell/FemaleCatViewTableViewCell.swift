//
//  FemaleCatViewTableViewCell.swift
//  Stray Spu
//
//  Created by Nattawut Nokyoo on 11/28/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import UIKit

class FemaleCatViewTableViewCell: UITableViewCell {
    @IBOutlet weak var myLabel: UILabel!
    @IBOutlet weak var myIdLabel: UILabel!
    @IBOutlet weak var myBreedLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
