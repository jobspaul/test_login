//
//  DataModelStatus.swift
//  Stray Spu
//
//  Created by Nattawut Nokyoo on 12/4/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import Foundation

class DataModelStatus: NSObject {
    
    
    var outComeType:String?
    var animalType:String?
    var outComeSubtype:String?
    var sexUponOutcome:String?
    var subTypeSex:String?
    var subTypeAge:String?
}

