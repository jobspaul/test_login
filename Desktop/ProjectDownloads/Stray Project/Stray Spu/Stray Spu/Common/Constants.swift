//
//  Constants.swift
//  Stray Spu
//
//  Created by Nattawut Nokyoo on 11/29/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import Foundation
struct Constants {
    //Lat long address
    static let latAddress = 51.481616
    static let longAdress = -0.017202
    
    //Call Center
    static let callCenter = "(800) 628-0028"
}
