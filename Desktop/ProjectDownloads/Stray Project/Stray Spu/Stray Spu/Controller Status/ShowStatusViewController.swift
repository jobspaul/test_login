//
//  ShowStatusViewController.swift
//  Stray Spu
//
//  Created by Nattawut Nokyoo on 12/4/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import UIKit

class ShowStatusViewController: UIViewController {
    
    var dataPass: DataModelStatus?
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Test:\(dataPass?.outComeType)")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
