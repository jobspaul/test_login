//
//  DataBaseViewController.swift
//  Stray Spu
//
//  Created by Nattawut Nokyoo on 11/27/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import UIKit
import Firebase

class DataBaseViewController: UIViewController {
    @IBOutlet weak var myLabel2: UILabel!
    @IBOutlet weak var myText: UITextView!
    
    override func viewDidLoad() {
      
        
        super.viewDidLoad()
        testdata()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func testdata() {
       var ref: DatabaseReference!
        ref = Database.database().reference()
      
        /*ref.observeSingleEvent(of: .value, with: { (dataSnapHot) in
            //debugPrint("Test Data : \(dataSnapHot)")
            print(dataSnapHot)
        }) { (error) in
            debugPrint("error : \(error.localizedDescription)")
        }*/
 
        ref.queryOrdered(byChild: "AnimalID").queryEqual(toValue: "A665666").observeSingleEvent(of: .value, with: {(Snap) in
            //debugPrint("Test Data 2 : \(Snap)")
            self.myLabel2.text = "\(Snap)"
            self.myText.text = "\(Snap)"
          //  if let snapDict = Snap.value as? [String:AnyObject]{
              //  for each in snapDict{
                    //print(each.0) // Will print out your node ID = 1
                    //print(each.1) //Will print out your Dictionary inside that node.
                    
                    //self.myLabel2.text = "\(each.1)"
                }
                
           // }
            
        )
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
