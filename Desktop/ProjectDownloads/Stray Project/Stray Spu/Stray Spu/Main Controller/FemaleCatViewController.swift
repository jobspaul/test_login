//
//  FemaleCatViewController.swift
//  Stray Spu
//
//  Created by Nattawut Nokyoo on 11/28/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import UIKit
import Firebase

class FemaleCatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var data: [DataModel] = []
    var ref: DatabaseReference!
    var dataSelect: DataModel?
    var selectData: DataModel?
    
    @IBOutlet weak var myTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.readText()
        self.title = "Breed"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func readText() {
        
        ref = Database.database().reference()
        ref.child("catF").observe(.childAdded, with: {(DataSnapshot) in
            //ref.observe(.childAdded, with: {(DataSnapshot) in
            //debugPrint("Test Data : \(DataSnapshot)")
            let value = DataSnapshot.value as? NSDictionary
            let username = value?["Name"] as? String ?? ""
            let userId = value?["AnimalID"] as? String ?? ""
            let userBreed = value?["Breed"] as? String ?? ""
            let userColer = value?["Color"] as? String ?? ""
            let userDate = value?["DateTime"] as? String ?? ""
            let userTime = value?["AgeuponOutcome"] as? String ?? ""
            let userSex = value?["SexuponOutcome"] as? String ?? ""
            //print("SHOW = \(String(describing: username))")
            
            let item = DataModel()
            item.myList2 = username
            item.myListId2 = userId
            item.myListBreed2 = userBreed
            item.myListColor2 = userColer
            item.myListDate2 = userDate
            item.myListTime2 = userTime
            item.myListSex2 = userSex
            self.data.append(item)
            
            
            self.myTableView.reloadData()
            
        }) { (error) in
            debugPrint("error : \(error.localizedDescription)")
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FemaleCatViewTableViewCell
        let item = self.data[indexPath.row]
        cell.myLabel.text = item.myListBreed2
        cell.myIdLabel.text = item.myListColor2
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        dataSelect = self.data[indexPath.row]
        self.performSegue(withIdentifier: "goToShowDetailCatF", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToShowDetailCatF" {
            if let vc = segue.destination as? ShowDetailCatFViewController {
                vc.dataPass = self.dataSelect
            }
        }
    }
}
