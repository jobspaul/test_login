//
//  MainMenuViewController.swift
//  Stray Spu
//
//  Created by Nattawut Nokyoo on 12/4/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import UIKit

class MainMenuViewController: UIViewController {
    @IBOutlet weak var statusBtn: UIButton!
    @IBOutlet weak var informationBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Main Menu"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionToInformation(_ sender: UIButton) {
        self.performSegue(withIdentifier: "goToInformation", sender: self)
    }
    
    @IBAction func actionToStatus(_ sender: UIButton) {
        self.performSegue(withIdentifier: "goToStatus", sender: self)
    }
    
    

}
