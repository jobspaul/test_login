//
//  DataTestModel.swift
//  Stray Spu
//
//  Created by Nattawut Nokyoo on 12/1/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import Foundation

class DataTestModel: NSObject {
    
    var outComeType1:String?
    var animalType1:String?
    var outComeSubtype1:String?
    var sexUponOutcome1:String?
    var subTypeSex1:String?
    var subTypeAge1:String?
}
