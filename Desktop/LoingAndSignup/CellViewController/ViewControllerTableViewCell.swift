//
//  ViewControllerTableViewCell.swift
//  LoingAndSignup
//
//  Created by Nattawut Nokyoo on 12/28/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import UIKit

class ViewControllerTableViewCell: UITableViewCell {
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelGenre: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
