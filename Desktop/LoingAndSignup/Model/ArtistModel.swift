//
//  ArtistModel.swift
//  LoingAndSignup
//
//  Created by Nattawut Nokyoo on 12/28/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import Foundation

class ArtistModel {
    
    var id: String?
    var name: String?
    var genre: String?
    
    init(id: String?, name: String?, genre: String?){
        self.id = id
        self.name = name
        self.genre = genre
    }
}
