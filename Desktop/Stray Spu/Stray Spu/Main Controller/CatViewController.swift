//
//  CatViewController.swift
//  Stray Spu
//
//  Created by Nattawut Nokyoo on 11/25/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import UIKit

class CatViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false;
        self.title = "Gender"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goCellCatMale(_ sender: UIButton) {
        self.performSegue(withIdentifier: "goToMaleCat", sender: self)
    }
    @IBAction func goCellCatFamale(_ sender: UIButton) {
        self.performSegue(withIdentifier: "goToMaleCatF", sender: self)
    }
    

}
