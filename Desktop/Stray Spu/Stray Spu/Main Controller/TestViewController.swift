//
//  TestViewController.swift
//  Stray Spu
//
//  Created by Nattawut Nokyoo on 12/1/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import UIKit
import Firebase

class TestViewController: UIViewController {
    @IBOutlet weak var showLabel: UILabel!
    
    var data2:[DataTestModel] = []
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setView () {
        ref = Database.database().reference()
        ref.child("dataAnimal").observe(.childAdded, with: {(DataSnapshot) in
            //ref.observe(.childAdded, with: {(DataSnapshot) in
            let value = DataSnapshot.value as? NSDictionary
            let type = value?["OutcomeType"] as? String ?? ""
            let animal = value?["AnimalType"] as? String ?? ""
            let subtype = value?["OutcomeSubtype"] as? String ?? ""
            let subtypeSex = value?["SexuponOutcome"] as? String ?? ""
            let subtypeAge = value?["AgeuponOutcome"] as? String ?? ""
            
            let item = DataTestModel()
            item.outComeType1 = type
            item.animalType1 = animal
            item.outComeSubtype1 = subtype
            item.sexUponOutcome1 = subtypeSex
            item.subTypeAge1 = subtypeAge
            self.data2.append(item)
            
            //CodeDataSai
            //DataAdoptio
            if (item.outComeType1 == "Adoption") {
                if (item.animalType1 == "Cat") {
                    //null
                   /* if (item.outComeSubtype1 == "null") {
                   
                        if (item.sexUponOutcome1 == "Neutered Male") {
              
                            if (item.subTypeAge1 == "2 years") {
                                print("Ans :\(item.subTypeAge1)")
                            
                            }
                        }
                        
                        else if (item.sexUponOutcome1 == "Spayed Female") {
                            
                                if (item.subTypeAge1 == "2 months") {
                                print("Ans2 :\(item.subTypeAge1)")
                            }
                        }
                    } */
                    if (item.outComeSubtype1 == "Foster") {
                        if (item.subTypeAge1 == "3 months") {
                                print("Ans3:\(item.subTypeAge1)")
                                self.showLabel.text = item.subTypeAge1
                            
                        }
                    }
                }
            }
            
            if (item.animalType1 == "Dog") {
                if (item.sexUponOutcome1 == "Neutered Male") {
                    //null
                   /* if (item.outComeSubtype1 == "null") {
                        
                        if (item.subTypeAge1 == "2 years") {
                            print("Ans4 :\(item.subTypeAge1)")
                            
                        }
                    } */
                    
                        
                    if (item.outComeSubtype1 == "Foster") {
                        if (item.subTypeAge1 == "1 year") {
                            print("Ans4:\(item.subTypeAge1)")
                        }
                    }
   
                }
                
                else if (item.sexUponOutcome1 == "Spayed Female") {
                    if (item.subTypeAge1 == "2 years") {
                        print("Ans5:\(item.subTypeAge1)")
                    }
                }
                
            }
            
            
            
            //DataEuthanasia
            if (item.outComeType1 == "Euthanasia") {
                if (item.outComeSubtype1 == "Aggressive") {
                    if (item.subTypeAge1 == "2 years") {
                        print("Ans6:\(item.subTypeAge1)")
                    }
                }
                
                else if (item.outComeSubtype1 == "Suffering") {
                    if (item.animalType1 == "Cat") {
                        if (item.subTypeAge1 == "1 year") {
                            print("Ans7:\(item.subTypeAge1)")
                        }
                    }
                    
                    if (item.animalType1 == "Dog") {
                        if (item.subTypeAge1 == "3 year") {
                            print("Ans8:\(item.subTypeAge1)")
                        }
                    }
                }
            }
            
                
            
            //DataReturn_to_owner
            if (item.outComeType1 == "Return_to_owner") {
                if (item.animalType1 == "Cat") {
                    if (item.subTypeAge1 == "1 year") {
                        print("Ans9:\(item.subTypeAge1)")
                    }
                }
                
                if (item.animalType1 == "Dog") {
                    if (item.sexUponOutcome1 == "Neutered Male") {
                        if (item.subTypeAge1 == "1 year") {
                            print("Ans10:\(item.subTypeAge1)")
                        }
                    }
                    
                    else if (item.sexUponOutcome1 == "Spayed Female") {
                        if (item.subTypeAge1 == "4 years") {
                            print("Ans11:\(item.subTypeAge1)")
                        }
                    }
                }
            }
            
                
            //DataTransfer
            else if (item.outComeType1 == "Transfer") {
                if (item.sexUponOutcome1 == "Intact Female") {
                    if (item.animalType1 == "Cat") {
                        if (item.subTypeAge1 == "3 weeks") {
                            print("Ans12:\(item.subTypeAge1)")
                        }
                    }
                    
                    if (item.animalType1 == "Dog") {
                        if (item.subTypeAge1 == "1 month") {
                            print("Ans13:\(item.subTypeAge1)")
                        }
                    }
                }
                
                else if (item.sexUponOutcome1 == "Intact Male") {
                    if (item.outComeSubtype1 == "Partner") {
                        if (item.animalType1 == "Cat") {
                            if (item.subTypeAge1 == "3 weeks") {
                                print("Ans14:\(item.subTypeAge1)")
                            }
                        }
                        
                        if (item.animalType1 == "Dog") {
                            if (item.subTypeAge1 == "1 year") {
                                print("Ans15:\(item.subTypeAge1)")
                            }
                        }
                    }
                }
                
                else if (item.sexUponOutcome1 == "Neutered Male") {
                    if (item.subTypeAge1 == "2 years") {
                        print("Ans16:\(item.subTypeAge1)")
                    }
                }
                
                else if (item.sexUponOutcome1 == "Spayed Female") {
                    if (item.animalType1 == "Cat") {
                        if (item.subTypeAge1 == "1 year") {
                            print("Ans17:\(item.subTypeAge1)")
                        }
                    }
                    
                    if (item.animalType1 == "Dog") {
                        if (item.subTypeAge1 == "2 years") {
                            print("Ans18:\(item.subTypeAge1)")
                        }
                    }
                }
                
                else if (item.sexUponOutcome1 == "Unknown") {
                    if (item.outComeSubtype1 == "Partner") {
                        if (item.subTypeAge1 == "2 weeks") {
                            print("Ans19:\(item.subTypeAge1)")
                        }
                    }
                    
                    else if (item.outComeSubtype1 == "SCRP") {
                        if (item.subTypeAge1 == "2 years") {
                            print("Ans20:\(item.subTypeAge1)")
                        }
                    }
                }
            }
            
            //Count data
            /*let a = [1,2,3]
            let a2 = [1,2,5,2,3]
            let size = a.count
            let size2 = a2.count
            if (size < size2) {
                print("True")
            }*/
            
            
            
        
           /* if (type == "Adoption") {
        
                if (animal == "cat") {
                   
                    if (subtypeSex == "Neutered Male") {
                      
                        if (subtype == "Foster") {
                            
                            self.showLabel.text = "test\(subtypeAge)"
                            
                        }
                    }
                
                }
               else if (type == "Adoption") {
                    
                    if (animal == "Dog") {
                        
                        if (subtypeSex == "Spayed Female") {
                            if (subtypeAge == "2 years") {
                                
                                self.showLabel.text = "test\(subtypeAge)"
                                //print("Error")
                            }
                        }
                        
                    }
                }
            } */
        
            
            
            
            //debugPrint("Test Data : \(DataSnapshot)")
        }) { (error) in
            debugPrint("error : \(error.localizedDescription)")
        }
    }
    
    
    
}

