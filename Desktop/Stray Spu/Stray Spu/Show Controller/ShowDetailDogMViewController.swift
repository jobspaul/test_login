//
//  ShowDetailDogMViewController.swift
//  Stray Spu
//
//  Created by Nattawut Nokyoo on 11/29/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import UIKit

class ShowDetailDogMViewController: UIViewController {
    var dataPass: DataModel?
    //var dataPass2: DataModel?
    @IBOutlet weak var showNameLabel: UILabel!
    @IBOutlet weak var showIdLabel: UILabel!
    @IBOutlet weak var showColorLabel: UILabel!
    @IBOutlet weak var showDateLabel: UILabel!
    @IBOutlet weak var showTimeOfDayLabel: UILabel!
    @IBOutlet weak var showSexLabel: UILabel!
    @IBOutlet weak var showBreedLabel: UILabel!
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
        showView()
        self.callToCallCenter()
        self.title = "Detail"
        // Do any additional setup after loading the view.
    }
    
    func setView () {
        
        showNameLabel.text = dataPass?.myList3
        showIdLabel.text = dataPass?.myListId3
        showColorLabel.text = dataPass?.myListColor3
        showDateLabel.text = dataPass?.myListDate3
        showTimeOfDayLabel.text = dataPass?.myListTime3
        showSexLabel.text = dataPass?.myListSex3
        showBreedLabel.text = dataPass?.myListBreed3
        //debugPrint(dataPass)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showView () {
        self.emailLabel.text = "solicitations@aspca.org"
        self.addressLabel.text = "Business Address : Second Floor, Pepys House, 10 Greenwich Quay, Clarence Rd, London SE8 3EY Post\nAddress : 163 Bellville House, 4 John Donne Way, London SE10 9FW"
        self.phoneLabel.text = Constants.callCenter
    }
    
    func callToCallCenter() {
        let tel = "tel://" + Constants.callCenter.replacingOccurrences(of: " ", with: "")
        UIApplication.shared.canOpenURL(URL(string: tel)!)
    }
}

