//
//  ShowDetailCatFViewController.swift
//  Stray Spu
//
//  Created by Nattawut Nokyoo on 11/28/17.
//  Copyright © 2017 Nattawut Nokyoo. All rights reserved.
//

import UIKit

class ShowDetailCatFViewController: UIViewController {
    @IBOutlet weak var showNameLabel: UILabel!
    @IBOutlet weak var showIdLabel: UILabel!
    @IBOutlet weak var showColorLabel: UILabel!
    @IBOutlet weak var showDateLabel: UILabel!
    @IBOutlet weak var showTimeOfDayLabel: UILabel!
    @IBOutlet weak var showSexLabel: UILabel!
    @IBOutlet weak var showBreedLabel: UILabel!
   
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    var dataPass: DataModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
        showView()
        self.callToCallCenter()
        self.title = "Detail"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setView () {
        showNameLabel.text = dataPass?.myList2
        showIdLabel.text = dataPass?.myListId2
        showColorLabel.text = dataPass?.myListColor2
        showDateLabel.text = dataPass?.myListDate2
        showTimeOfDayLabel.text = dataPass?.myListTime2
        showSexLabel.text = dataPass?.myListSex2
        showBreedLabel.text = dataPass?.myListBreed2
       // debugPrint(dataPass?.myList2)
        
    }
    
    func showView () {
        self.emailLabel.text = "solicitations@aspca.org"
        self.addressLabel.text = "Business Address : Second Floor, Pepys House, 10 Greenwich Quay, Clarence Rd, London SE8 3EY Post\nAddress : 163 Bellville House, 4 John Donne Way, London SE10 9FW"
        self.phoneLabel.text = Constants.callCenter
    }
    
    func callToCallCenter() {
        let tel = "tel://" + Constants.callCenter.replacingOccurrences(of: " ", with: "")
        UIApplication.shared.canOpenURL(URL(string: tel)!)
    }
}
